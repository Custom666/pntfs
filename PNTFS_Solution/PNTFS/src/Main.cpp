#include <iostream>
#include <sstream>
#include <vector>
#include <iterator> 
#include "Filesystem.h"
#include <fstream>
#include <future>

#include "spdlog/spdlog.h"

#ifndef LogAndExit

#define LogAndExit(logger, msg) ({logger->error((msg)); return EXIT_FAILURE;})

#endif

std::vector<std::string> Split(std::string input, const std::string delimiter)
{
	size_t pos = 0;
	std::string token;
	std::vector<std::string> result;

	while ((pos = input.find(delimiter)) != std::string::npos) 
	{
		token = input.substr(0, pos);
			
		result.push_back(token);

		input.erase(0, pos + delimiter.length());
	}

	result.push_back(input);

	return result;
}

void proceed(std::vector<std::string> parameters, Filesystem* fs)
{
	auto logger = spdlog::get("all");

	if (parameters[0].compare("format") == 0)
	{
		fs->Format(std::stoi(parameters[1]), parameters[2]);
	}
	else if (parameters[0].compare("ls") == 0)
	{
		std::vector<std::string> arguments;

		if (parameters.size() > 1) arguments = Split(parameters[1], "/");

		fs->ListDirectory(arguments);
	}
	else if (parameters[0].compare("mkdir") == 0)
	{
		fs->MakeDirectory(Split(parameters[1], "/"));
	}
	else if (parameters[0].compare("cd") == 0)
	{
		fs->ChangeDirectory(Split(parameters[1], "/"));
	}
	else if (parameters[0].compare("rmdir") == 0)
	{
		fs->RemoveDirectory(Split(parameters[1], "/"));
	}
	else if (parameters[0].compare("pwd") == 0)
	{
		std::cout << fs->PrintWorkingDirectory() << std::endl;
	}
	else if (parameters[0].compare("info") == 0)
	{
		std::cout << fs->Info(Split(parameters[1], "/")) << std::endl;
	}
	else if (parameters[0].compare("incp") == 0)
	{
		fs->InCopy(Split(parameters[1], "/"), Split(parameters[2], "/"));
	}
	else if (parameters[0].compare("outcp") == 0)
	{
		fs->OutCopy(Split(parameters[1], "/"), Split(parameters[2], "/"));
	}
	else if (parameters[0].compare("rm") == 0)
	{
		fs->RemoveFile(Split(parameters[1], "/"));
	}
	else if (parameters[0].compare("cat") == 0)
	{
		std::cout << fs->ConcatenateAndPrint(Split(parameters[1], "/")) << std::endl;
	}
	else if (parameters[0].compare("cp") == 0)
	{
		fs->Copy(Split(parameters[1], "/"), Split(parameters[2], "/"));
	}
	else if (parameters[0].compare("mv") == 0)
	{
		fs->Move(Split(parameters[1], "/"), Split(parameters[2], "/"));
	}
	else if (parameters[0].compare("load") == 0)
	{
		std::ifstream ifile(parameters[1]);
		std::string command;

		if (!ifile.is_open())
		{
			std::cout << "FILE NOT FOUND (nen� zdroj)" << std::endl;
			logger->error("FILE NOT FOUND [{}]", parameters[1]);

			return;
		}

		std::cout << "OK" << std::endl;

		while (!ifile.eof())
		{
			getline(ifile, command);
			
			logger->info("Processing command [{}]", command);
			std::cout << command << std::endl;

			proceed(Split(command, " "), fs);
		}
	}
	else if (parameters[0].compare("check") == 0)
	{
		fs->Check();
	}
	else if (parameters[0].compare("corrupt") == 0)
	{
		fs->CorruptSize(Split(parameters[1], "/"));
	}
}

int main(int argc, char* argv[])
{
	std::shared_ptr<spdlog::logger> logger;
	std::string input;

	logger = spdlog::rotating_logger_mt("all", "all.txt", 1048576 * 20, 3);
	logger->flush_on(spdlog::level::info);

	//logger = spdlog::stdout_logger_mt("all");

	if (argc < 2) LogAndExit(logger, "Wrong parameter count! Process will be terminated...");

	auto filename = std::string(argv[1]);

	if (filename.empty()) LogAndExit(logger, "Filename cannot be empty!");

	auto fs = new Filesystem(filename);

	//if (!fs->Initialize()) // file not exist
	//{
	//	std::cout << "Enter size [MB]: ";
	//	std::cin >> size;

	//	fs->Format(size, "MB");
	//}

	while (input != "exit") // handle user inputs
	{
		std::cout << fs->PrintWorkingDirectory() << "$ " << std::flush;

		std::getline(std::cin, input);

		if (input.empty()) continue;

		logger->info("Input commnand [{}]", input);

		proceed(Split(input, " "), fs);
	}

	delete fs;

	return EXIT_SUCCESS;
}