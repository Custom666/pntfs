﻿#include <fstream>
#include <sstream>
#include <iterator>
#include <iostream>
#include <errno.h>
#include <algorithm>
#include <memory>
#include <cstring>
#include <thread>
#include <future>

#include "Filesystem.h"
#include "spdlog/spdlog.h"

std::string Filesystem::_filename = std::string();

Filesystem::Filesystem(std::string filename)
{
	_filename = filename;

	auto logger = spdlog::get("all");

	logger->info("Opening file {}...", filename);

	std::ifstream file(filename, std::ios::in | std::ios::binary);

	if (!file.is_open())
	{
		logger->warn("Cannot open file [{}] !", filename);

		return;
	}

	file.seekg(0);

	file.read((char*)&_bootRecord, sizeof(boot_record));

	_mftLength = (_bootRecord.bitmap_start_address - _bootRecord.mft_start_address) / sizeof(mft_item);

	_mftTable = new mft_item[_mftLength];

	file.read((char*)_mftTable, sizeof(mft_item) * _mftLength);

	_bitmapLength = (_bootRecord.data_start_address - _bootRecord.bitmap_start_address);

	_bitmap = new bool[_bitmapLength];

	file.read((char*)_bitmap, sizeof(bool) * _bitmapLength);

	file.close();

	logger->info(_bootRecord.ToString());
	logger->info("mft items count: {}", _mftLength);
	logger->info("bitmap length: {}", _bitmapLength);
	logger->info("File {} opened succesfully.", filename);
}

Filesystem::~Filesystem()
{
}

bool Filesystem::Format(uint32_t size, std::string units)
{
	auto logger = spdlog::get("all");

	// convert size to Bytes
	if (units.compare("KB") == 0) size *= 1024;
	else if (units.compare("MB") == 0) size *= 1024 * 1024;
	else if (units.compare("GB") == 0) size *= 1024 * 1024 * 1024;

	auto sizeofMftItem = sizeof(mft_item);

	_mftLength = ((size / (uint32_t)100) * (uint32_t)10) / sizeofMftItem;
	uint64_t mftSize = _mftLength * sizeofMftItem;

	_mftTable = new mft_item[_mftLength];

	auto bitmapAndDataSize = size - (mftSize + sizeof(boot_record));

	uint32_t s = 0;
	_bitmapLength = 0;

	while ((s + _bitmapLength) < bitmapAndDataSize)
	{
		s += CLUSTER_SIZE;

		if ((s + _bitmapLength) > bitmapAndDataSize)
		{
			s -= CLUSTER_SIZE;
			break;
		}

		_bitmapLength++;
	}

	std::strcpy(_bootRecord.signature, "");
	std::strcpy(_bootRecord.volume_descriptor, "");
	_bootRecord.disk_size = size;
	_bootRecord.cluster_size = CLUSTER_SIZE;
	_bootRecord.cluster_count = (uint32_t)_bitmapLength;
	_bootRecord.mft_start_address = sizeof(boot_record);
	_bootRecord.bitmap_start_address = sizeof(boot_record) + mftSize;
	_bootRecord.data_start_address = sizeof(boot_record) + mftSize + _bitmapLength;
	_bootRecord.mft_max_fragment_count = MFT_FRAGMENTS_COUNT;

	std::ofstream ofile(_filename, std::ios::binary | std::ios::out);

	if (!ofile)
	{
		logger->error("Cannot create file [{}] !", _filename);
		std::cout << "CANNOT CREATE FILE" << std::endl;

		return false;
	}

	ofile.seekp(0);

	ofile.write((char*)&_bootRecord, sizeof(boot_record)); // write boot record

	_mftTable[0].uid = 1;
	_mftTable[0].isDirectory = true;
	_mftTable[0].item_order = 1;
	_mftTable[0].item_order_total = 1;
	strcpy(_mftTable[0].item_name, "root");
	_mftTable[0].item_size = sizeof(int32_t);
	_mftTable[0].fragments[0] = { _bootRecord.data_start_address, 1 };

	ofile.write((const char*)_mftTable, sizeof(mft_item) * _mftLength); // write mft table

	_bitmap = new bool[_bitmapLength];

	_bitmap[0] = true;

	ofile.write((const char*)_bitmap, sizeof(bool) * _bitmapLength); // write bitmap

	ofile.write((const char*)&_mftTable[0].uid, sizeof(int32_t)); // write root directory

	ofile.seekp(size - 1);
	ofile.write("", 1);

	ofile.close();

	std::cout << "OK" << std::endl;

	return true;
}

void Filesystem::ListDirectory(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	if (!path.empty() && path.back().empty()) path.pop_back();

	auto dir = pathExist(path);

	bool hasError = dir.empty();

	if (!hasError) for (auto item : dir) if (!item->isDirectory) hasError = true;

	if (hasError)
	{
		std::ostringstream oss;
		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		std::cout << "PATH NOT FOUND (neexistující adresář)" << std::endl;

		logger->error("PATH [{}] NOT FOUND!", oss.str());
	}

	for (auto item : getDirectoryContent(dir))
	{
		std::string itemInfo = "";

		itemInfo += item[0]->isDirectory ? "+" : "-";
		itemInfo += item[0]->item_name;

		std::cout << itemInfo << std::endl;
		logger->info(itemInfo);
	}
}

void Filesystem::ChangeDirectory(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	auto dir = pathExist(path);

	bool hasError = dir.empty();

	if (!hasError) for (auto item : dir) if (!item->isDirectory) hasError = true;

	if (hasError)
	{
		std::ostringstream oss;
		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		std::cout << "PATH NOT FOUND (neexistující cesta)" << std::endl;

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	_currentDirectory = dir[0]->uid;

	std::cout << "OK" << std::endl;
}

void Filesystem::MakeDirectory(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	auto newDirName = path[path.size() - 1];

	path.pop_back();

	auto currentDir = pathExist(path);

	if (currentDir.empty())
	{
		std::cout << "PATH NOT FOUND (neexistuje zadaná cesta)" << std::endl;

		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	if (!(itemExist(newDirName, currentDir).empty()))
	{
		std::cout << "EXIST (nelze založit, již existuje)" << std::endl;

		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("[{}] ALREADY EXIST!", oss.str());

		return;
	}

	createMftItem(newDirName, true, currentDir, (char*)&currentDir[0]->uid, sizeof(uint32_t));
}

void Filesystem::RemoveDirectory(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	uint64_t totalSize = 0;
	std::vector<mft_item*> currentDir = pathExist(path);

	if (currentDir.empty() || !currentDir[0]->isDirectory)
	{
		std::cout << "FILE NOT FOUND (neexistující adresář)" << std::endl;

		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	for (auto const& item : currentDir) totalSize += item->item_size;

	if (totalSize > sizeof(uint32_t))
	{
		std::cout << "NOT EMPTY (adresář obsahuje podadresáře, nebo soubory)" << std::endl;

		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("DIRECTORY [{}] NOT EMPTY!", oss.str());

		return;
	}

	removeMftItem(currentDir, getDirectoryContent(currentDir)[0]);

	std::cout << "OK" << std::endl;
}

std::string Filesystem::PrintWorkingDirectory()
{
	std::vector<std::string> result;
	std::stringstream ss;

	auto currentDir = GetCurrentDirectory();

	if (currentDir.empty()) return "";

	result.push_back(currentDir[0]->item_name);

	while (_mftTable[0].uid != currentDir[0]->uid)
	{
		auto content = getDirectoryContent(currentDir);

		currentDir = content[0];

		result.push_back(currentDir[0]->item_name);
	}

	std::reverse(result.begin(), result.end());

	for (auto it = result.begin(); it != result.end(); ++it) ss << "/" << *it;

	ss << std::flush;

	return ss.str();
}

std::string Filesystem::Info(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");
	std::ostringstream oss;

	auto item = pathExist(path);

	if (item.empty())
	{
		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return "FILE NOT FOUND (není zdroj)";
	}

	for (auto const& mftItem : item)
	{
		oss << item[0]->item_name << " - " << item[0]->uid << " - " << mftItem->item_size << std::endl;

		for (auto const& fragment : mftItem->fragments) if (fragment.fragment_count > 0) oss << fragment.fragment_count << " - " << fragment.fragment_start_address << std::endl;
	}

	return oss.str();
}

void Filesystem::InCopy(std::vector<std::string> source, std::vector<std::string> destination)
{
	auto logger = spdlog::get("all");

	std::ostringstream oss;
	std::string destinationFileName = destination.back();

	destination.pop_back();

	std::copy(source.begin(), source.end(), std::ostream_iterator<std::string>(oss, "/"));

	std::string sourceFileName = oss.str();
	sourceFileName.pop_back();

	std::ifstream sourceFile(sourceFileName, std::ios::binary | std::ios::in);

	if (!sourceFile.is_open())
	{
		std::cout << "FILE NOT FOUND (není zdroj)" << std::endl;
		logger->error("FILE [{}] NOT FOUND", sourceFileName);
		return;
	}

	auto destinationParentDirectory = pathExist(destination);

	if (destinationParentDirectory.empty())
	{
		std::cout << "PATH NOT FOUND (neexistuje cílová cesta)" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND", oss.str());

		return;
	}

	if (!itemExist(destinationFileName, destinationParentDirectory).empty())
	{
		std::cout << "FILE ALREADY EXIST IN GIVEN DIRECTORY" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("FILE [{}] ALREADY EXIST IN [{}]", destinationFileName, oss.str());

		return;
	}

	std::vector<unsigned char> data(std::istreambuf_iterator<char>(sourceFile), {});

	sourceFile.close();

	createMftItem(destinationFileName, false, destinationParentDirectory, (char*)&data[0], data.size() * sizeof(unsigned char));
}

void Filesystem::OutCopy(std::vector<std::string> source, std::vector<std::string> destination)
{
	auto logger = spdlog::get("all");
	std::ostringstream oss;

	auto sourceFile = pathExist(source);

	if (sourceFile.empty())
	{
		std::cout << "PATH NOT FOUND (neexistuje cílová cesta)" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND", oss.str());

		return;
	}

	std::ifstream file(_filename, std::ios::binary | std::ios::in);

	std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

	std::string destinationFileName = oss.str();
	destinationFileName.pop_back();

	std::ofstream destinationFile(destinationFileName, std::ios::binary | std::ios::out);

	if (!destinationFile.good())
	{
		std::cout << "FILE NOT FOUND (není zdroj)" << std::endl;

		logger->error("FILE [{}] NOT FOUND", destinationFileName);

		return;
	}

	auto content = getFileContent(sourceFile);

	destinationFile.seekp(0);
	destinationFile.write((char*)&content[0], content.size());

	destinationFile.close();

	std::cout << "OK" << std::endl;
}

void Filesystem::RemoveFile(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	auto file = pathExist(path);

	if (file.empty() || file[0]->isDirectory)
	{
		std::cout << "FILE NOT FOUND" << std::endl;

		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	path.pop_back();

	auto parentDirectory = pathExist(path);

	removeMftItem(file, parentDirectory.empty() ? GetCurrentDirectory() : parentDirectory);

	std::cout << "OK" << std::endl;
}

std::string Filesystem::ConcatenateAndPrint(std::vector<std::string> path)
{
	auto logger = spdlog::get("all");

	auto file = pathExist(path);

	if (file.empty())
	{
		std::ostringstream oss;

		std::copy(path.begin(), path.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return "FILE NOT FOUND (není zdroj)";
	}

	auto content = getFileContent(file);

	return std::string(content.begin(), content.end());
}

void Filesystem::Copy(std::vector<std::string> source, std::vector<std::string> destination)
{
	auto logger = spdlog::get("all");

	std::string destinationFileName = destination.back();

	destination.pop_back();

	auto destinationParentDirectory = pathExist(destination);
	auto sourceFile = pathExist(source);

	if (sourceFile.empty())
	{
		std::cout << "FILE NOT FOUND (není zdroj)" << std::endl;

		std::ostringstream oss;

		std::copy(source.begin(), source.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	if (destinationParentDirectory.empty())
	{
		std::cout << "PATH NOT FOUND (neexistuje cílová cesta)" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	if (!itemExist(destinationFileName, destinationParentDirectory).empty())
	{
		std::cout << "FILE ALREADY EXIST IN GIVEN DIRECTORY" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("FILE [{}] ALREADY EXIST IN [{}]", destinationFileName, oss.str());

		return;
	}

	auto data = getFileContent(sourceFile);

	createMftItem(destinationFileName, false, destinationParentDirectory, (char*)&data[0], data.size() * sizeof(unsigned char));
}

void Filesystem::Move(std::vector<std::string> source, std::vector<std::string> destination)
{
	auto logger = spdlog::get("all");

	uint32_t i = 0; uint32_t j = 0; uint64_t totalFragmentSize;
	std::vector<uint32_t> sourceParentDirectoryContent;
	bool itemToRemoveFoundInParent = false;
	std::vector<uint32_t> rootDirFragmentContent;
	std::string destinationFileName = destination.back();

	destination.pop_back();

	auto destinationParentDirectory = pathExist(destination);
	auto sourceFile = pathExist(source);

	if (sourceFile.empty())
	{
		std::cout << "FILE NOT FOUND (není zdroj)" << std::endl;

		std::ostringstream oss;

		std::copy(source.begin(), source.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	if (destinationParentDirectory.empty())
	{
		std::cout << "PATH NOT FOUND (neexistuje cílová cesta)" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("PATH [{}] NOT FOUND!", oss.str());

		return;
	}

	if (!itemExist(destinationFileName, destinationParentDirectory).empty())
	{
		std::cout << "FILE ALREADY EXIST IN GIVEN DIRECTORY" << std::endl;

		std::ostringstream oss;

		std::copy(destination.begin(), destination.end(), std::ostream_iterator<std::string>(oss, "/"));

		logger->error("FILE [{}] ALREADY EXIST IN [{}]", destinationFileName, oss.str());

		return;
	}

	source.pop_back();
	auto sourceParentDirectory = pathExist(source);

	std::fstream file(_filename, std::ios::binary | std::ios::in | std::ios::out);

	// remove from parent directory
	for (i = 0; i < sourceParentDirectory.size(); i++)
	{
		for (j = 0; j < _bootRecord.mft_max_fragment_count; j++)
		{
			if (sourceParentDirectory[i]->fragments[j].fragment_count <= 0) continue;

			file.seekg(sourceParentDirectory[i]->fragments[j].fragment_start_address);

			auto s = _bootRecord.cluster_size * (sourceParentDirectory[i]->fragments[j].fragment_count);

			totalFragmentSize = sourceParentDirectory[i]->item_size > s ? s : sourceParentDirectory[i]->item_size;

			auto length = totalFragmentSize / sizeof(uint32_t);

			rootDirFragmentContent.clear();

			for (uint32_t k = 0; k < length; k++)
			{
				uint32_t uid;

				file.read((char*)&uid, sizeof(uint32_t));

				rootDirFragmentContent.push_back(uid);

				itemToRemoveFoundInParent = uid == sourceFile[0]->uid;
			}

			if (itemToRemoveFoundInParent) break;
		}
		if (itemToRemoveFoundInParent) break;
	}

	rootDirFragmentContent.erase(std::remove(rootDirFragmentContent.begin(), rootDirFragmentContent.end(), sourceFile[0]->uid), rootDirFragmentContent.end());

	char zero[totalFragmentSize];
	std::fill_n(zero, totalFragmentSize, 0);

	file.seekp(sourceParentDirectory[i]->fragments[j].fragment_start_address);
	file.write((char*)&zero, totalFragmentSize);
	file.seekp(sourceParentDirectory[i]->fragments[j].fragment_start_address);
	for (auto const& u : rootDirFragmentContent) file.write((char*)&u, sizeof(uint32_t));

	sourceParentDirectory[i]->item_size -= sizeof(uint32_t);

	// rename source file
	for (auto const& mftItem : sourceFile)
	{
		std::fill_n(mftItem->item_name, 12, '\0');
		std::strncpy(mftItem->item_name, destinationFileName.c_str(), 12);
	}

	// move source file into destination directory
	file.seekp(destinationParentDirectory.back()->fragments[0].fragment_start_address + destinationParentDirectory.back()->item_size);
	file.write((char*) &(sourceFile[0]->uid), sizeof(uint32_t));
	destinationParentDirectory.back()->item_size += sizeof(uint32_t);

	// update mft table
	file.seekp(_bootRecord.mft_start_address);
	file.write((char*)_mftTable, sizeof(mft_item) * _mftLength);

	std::cout << "OK" << std::endl;
}

void Filesystem::Check()
{
	std::vector<std::future<std::string>> futures;
	std::stringstream ss;
	std::vector<std::vector<mft_item>> items;

	for (uint32_t i = 0; i < _mftLength; i++)
	{
		auto item = _mftTable[i];

		if (item.uid <= 0) continue;

		bool exist = false;

		for (auto existItem : items)
		{
			if (existItem[0].uid == item.uid)
			{
				exist = true;
				existItem.push_back(item);
			}
		}

		if (!exist) items.push_back(std::vector<mft_item> { item });
	}

	for (auto const& item : items)
	{
		futures.push_back(std::async(std::launch::async, check, item, items));

		//ss << check(item, items);
	}

	for (auto& future : futures) ss << future.get();

	auto result = ss.str();

	std::cout << (result.empty() ? "OK" : result) << std::endl;
}

void Filesystem::CorruptSize(std::vector<std::string> path)
{
	uint64_t totalItemSize = 0;
	uint64_t totalFragmentSize = 0;

	auto item = pathExist(path);

	// corrupt fragment item size

	for (auto const& i : item)
	{
		totalItemSize += i->item_size;

		for (auto const& f : i->fragments) if (f.fragment_count > 0) totalFragmentSize += (f.fragment_count * _bootRecord.cluster_size);
	}

	uint64_t i = item.size() - 1;
	uint32_t j = _bootRecord.mft_max_fragment_count;

	std::fstream file(_filename, std::ios::binary | std::ios::in | std::ios::out);

	do
	{
		j--;

		if (j < 0)
		{
			i--;
			j = _bootRecord.mft_max_fragment_count;
		}

		if (item[i]->fragments[j].fragment_count > 0)
		{
			char z[item[i]->fragments[j].fragment_count * _bootRecord.cluster_size];
			std::fill_n(z, item[i]->fragments[j].fragment_count * _bootRecord.cluster_size, 0);

			file.seekp(item[i]->fragments[j].fragment_start_address);
			file.write(z, item[i]->fragments[j].fragment_count * _bootRecord.cluster_size);

			totalFragmentSize -= (item[i]->fragments[j].fragment_count * _bootRecord.cluster_size);

			item[i]->fragments[j].fragment_count = 0;
			item[i]->fragments[j].fragment_start_address = 0;
		}

	} while (!item.empty() && totalFragmentSize >= totalItemSize);

	// write mft table changes
	file.seekp(_bootRecord.mft_start_address);
	file.write((char*)_mftTable, sizeof(mft_item) * _mftLength);

	file.close();
}

std::vector<mft_item*> Filesystem::GetCurrentDirectory()
{
	if (_currentDirectory <= 0) _currentDirectory = 1;

	return getMftItem(_currentDirectory);
}

std::vector<mft_item*> Filesystem::pathExist(std::vector<std::string> path)
{
	if (path.empty()) return  GetCurrentDirectory();

	// get root directory of absolute or relative path
	std::vector<mft_item*> root = path[0].empty() ? getMftItem(1) : GetCurrentDirectory();

	if (path[0].empty()) path.erase(path.begin());

	for (auto itemName : path)
	{
		root = itemExist(itemName, root);

		if (root.empty() && !path.back().empty()) return std::vector<mft_item*>();
	}

	return root;
}

std::vector<mft_item*> Filesystem::itemExist(std::string itemName, std::vector<mft_item*> relativeTo)
{
	for (auto const& item : getDirectoryContent(relativeTo)) if (itemName.compare(std::string(item[0]->item_name)) == 0) return item;

	return std::vector<mft_item*>();
}

std::vector<mft_item*> Filesystem::getMftItem(uint32_t uid)
{
	std::vector<mft_item*> result;

	for (uint32_t index = 0; index < _mftLength; index++) if (_mftTable[index].uid == uid) result.push_back(&_mftTable[index]);

	return result;
}

void Filesystem::createMftItem(std::string name, bool isDirectory, std::vector<mft_item*> parentDirectory, char * data, uint64_t dataLength)
{
	auto logger = spdlog::get("all");
	std::map<uint32_t, uint32_t> freeIndexedAndLength;
	std::map<uint32_t, uint32_t>::iterator it;
	std::vector<uint32_t> freeIndexesInBitmap;
	std::vector<mft_item*> freeItems;
	uint32_t j = 0;
	uint64_t writenDataLength = 0;

	// Find free bit length in bitmap
	auto neededBitLength = (dataLength / _bootRecord.cluster_size) + 1;

	for (uint32_t i = 0; i < _bitmapLength && j < neededBitLength; i++)
	{
		if (_bitmap[i] == false)
		{
			// More fragments needed => store start free index into map with length
			if (!freeIndexesInBitmap.empty() && (i - freeIndexesInBitmap.back()) > 1)
			{
				freeIndexedAndLength.insert(std::pair<uint32_t, uint32_t>(freeIndexesInBitmap[0], freeIndexesInBitmap.size()));

				freeIndexesInBitmap.clear();
			}

			freeIndexesInBitmap.push_back(i);

			j++;
		}
	}

	freeIndexedAndLength.insert(std::pair<uint32_t, uint32_t>(freeIndexesInBitmap[0], freeIndexesInBitmap.size()));

	if (j != neededBitLength)
	{
		logger->error("Creating new mft item [{}] failed. No free space found! Needed {} bytes but founded only {}.", name, neededBitLength, freeIndexesInBitmap.size());

		std::cout << "No free space found!" << std::endl;

		return;
	}

	// Find free mft item in mft table
	uint32_t mftItemsCount = ((uint32_t)freeIndexedAndLength.size() / _bootRecord.mft_max_fragment_count) + 1;

	for (uint32_t index = 0; index < _mftLength && freeItems.size() < mftItemsCount; index++) if (_mftTable[index].uid == 0) freeItems.push_back(&_mftTable[index]);

	if (freeItems.size() != mftItemsCount)
	{
		logger->error("Creating new mft item [{}] failed. No free mft item found! Needed {} items but founded only {}.", name, mftItemsCount, freeItems.size());

		std::cout << "No free mft item found!" << std::endl;

		return;
	}

	// Write information into mft item
	std::fstream file(_filename, std::ios::out | std::ios::in | std::ios::binary);

	auto uid = generateUID();

	for (uint32_t i = 0; i < freeItems.size(); i++)
	{
		// create fragments
		j = 0; writenDataLength = 0;

		for (it = freeIndexedAndLength.begin(); it != freeIndexedAndLength.end() && j < _bootRecord.mft_max_fragment_count; ++it)
		{
			auto freeBitmapIndex = (*it).first;
			auto clusterCount = (*it).second;

			freeItems[i]->fragments[j].fragment_start_address = _bootRecord.data_start_address + (freeBitmapIndex * _bootRecord.cluster_size);
			freeItems[i]->fragments[j].fragment_count = clusterCount;

			// write data
			auto length = dataLength > (_bootRecord.cluster_size * clusterCount) ? (_bootRecord.cluster_size * clusterCount) : dataLength;

			file.seekp(freeItems[i]->fragments[j].fragment_start_address);
			file.write((data + writenDataLength), length);

			writenDataLength += length;

			// change bitmap
			for (uint32_t k = 0; k < clusterCount; k++) _bitmap[freeBitmapIndex + k] = true;

			j++;
		}

		freeIndexedAndLength.erase(freeIndexedAndLength.begin(), it);

		freeItems[i]->uid = uid;
		freeItems[i]->isDirectory = isDirectory;
		strcpy(freeItems[i]->item_name, name.c_str());
		freeItems[i]->item_order = (i + 1);
		freeItems[i]->item_order_total = mftItemsCount;
		freeItems[i]->item_size = writenDataLength;
	}

	// write item into current dir
	file.seekp(parentDirectory.back()->fragments[0].fragment_start_address + parentDirectory.back()->item_size);
	file.write((char*)&freeItems[0]->uid, sizeof(uint32_t));
	parentDirectory.back()->item_size += sizeof(uint32_t); // TODO co kdyz se nevejde do clusteru ? 

	// write mft
	file.seekp(_bootRecord.mft_start_address);
	file.write((char*)_mftTable, sizeof(mft_item) * _mftLength);

	// write bitmap
	file.seekp(_bootRecord.bitmap_start_address);
	file.write((char*)_bitmap, sizeof(bool) * _bitmapLength);

	file.close();

	std::cout << "OK" << std::endl;
}

int32_t Filesystem::generateUID()
{
	uint32_t result = 0;

	for (uint32_t i = 0; i < _mftLength; i++) if (_mftTable[i].uid > result) result = _mftTable[i].uid;

	return ++result;
}

void Filesystem::removeMftItem(std::vector<mft_item*> itemToRemove, std::vector<mft_item*> parentDirectory)
{
	uint64_t totalFragmentSize;
	uint32_t i = 0; uint32_t j = 0;
	bool itemToRemoveFoundInParent = false;
	std::vector<uint32_t> rootDirFragmentContent;
	char emptyName[12];
	std::fill_n(emptyName, 12, '\0');

	std::fstream file(_filename, std::ios::binary | std::ios::in | std::ios::out);

	// remove from parent directory
	for (i = 0; i < parentDirectory.size(); i++)
	{
		for (j = 0; j < _bootRecord.mft_max_fragment_count; j++)
		{
			if (parentDirectory[i]->fragments[j].fragment_count <= 0) continue;

			file.seekg(parentDirectory[i]->fragments[j].fragment_start_address);

			auto s = _bootRecord.cluster_size * (parentDirectory[i]->fragments[j].fragment_count);

			totalFragmentSize = parentDirectory[i]->item_size > s ? s : parentDirectory[i]->item_size;

			auto length = totalFragmentSize / sizeof(uint32_t);

			rootDirFragmentContent.clear();

			for (uint32_t k = 0; k < length; k++)
			{
				uint32_t uid;

				file.read((char*)&uid, sizeof(uint32_t));

				rootDirFragmentContent.push_back(uid);

				itemToRemoveFoundInParent = uid == itemToRemove[0]->uid;
			}

			if (itemToRemoveFoundInParent) break;
		}
		if (itemToRemoveFoundInParent) break;
	}

	rootDirFragmentContent.erase(std::remove(rootDirFragmentContent.begin(), rootDirFragmentContent.end(), itemToRemove[0]->uid), rootDirFragmentContent.end());

	char zero[totalFragmentSize];
	std::fill_n(zero, totalFragmentSize, 0);

	file.seekp(parentDirectory[i]->fragments[j].fragment_start_address);
	file.write((char*)&zero, totalFragmentSize);
	file.seekp(parentDirectory[i]->fragments[j].fragment_start_address);
	for (auto const& u : rootDirFragmentContent) file.write((char*)&u, sizeof(uint32_t));

	parentDirectory[i]->item_size -= sizeof(uint32_t);

	// remove from data section
	for (auto const& mftItem : itemToRemove)
	{
		for (auto const& fragment : mftItem->fragments)
		{
			if (fragment.fragment_count <= 0) continue;

			auto len = fragment.fragment_count * _bootRecord.cluster_size;

			char z[len];
			std::fill_n(z, len, 0);

			file.seekp(fragment.fragment_start_address);
			file.write((char*)&z, len);

			// remove from bitmap
			auto bitmapIndex = (fragment.fragment_start_address - _bootRecord.data_start_address) / _bootRecord.cluster_size;
			for (i = 0; i < fragment.fragment_count; i++) _bitmap[bitmapIndex + i] = false;
		}

		// remove from mft table
		mftItem->uid = 0;
		mftItem->isDirectory = false;
		mftItem->item_order = 1;
		mftItem->item_order_total = 1;
		mftItem->item_size = 0;
		std::strncpy(mftItem->item_name, emptyName, 12);

		for (uint32_t i = 0; i < _bootRecord.mft_max_fragment_count; i++)
		{
			mftItem->fragments[i].fragment_count = 0;
			mftItem->fragments[i].fragment_start_address = 0;
		}
	}

	// write bitmap changes
	file.seekp(_bootRecord.bitmap_start_address);
	file.write((char*)_bitmap, sizeof(bool) * _bitmapLength);

	// write mft table changes
	file.seekp(_bootRecord.mft_start_address);
	file.write((char*)_mftTable, sizeof(mft_item) * _mftLength);

	file.close();
}

std::vector<std::vector<mft_item*>> Filesystem::getDirectoryContent(std::vector<mft_item*> directory)
{
	std::vector<std::vector<mft_item*>>  result;

	std::ifstream ifile(_filename, std::ios::binary | std::ios::in);

	for (auto const& mftItem : directory)
	{
		auto totalSize = mftItem->item_size;

		for (auto const& fragment : mftItem->fragments)
		{
			if (fragment.fragment_count > 0)
			{
				ifile.seekg(fragment.fragment_start_address);

				uint64_t totalFragmentSize = _bootRecord.cluster_size * fragment.fragment_count;

				if (totalSize > totalFragmentSize) totalSize -= totalFragmentSize;

				uint64_t s = (totalSize > totalFragmentSize ? totalFragmentSize : totalSize);

				uint64_t length =  s / sizeof(uint32_t);

				int32_t uids[length];

				ifile.read((char*)&uids, s);

				for (auto const& uid : uids)
				{
					auto item = getMftItem(uid);

					if(!item.empty()) result.push_back(item);
				}
			}
		}

	}

	ifile.close();

	return result;
}

std::vector<char> Filesystem::getFileContent(std::vector<mft_item*> file)
{
	std::vector<char> result;

	std::ifstream ifile(_filename, std::ios::binary | std::ios::in);

	for (auto const& mftItem : file)
	{
		for (auto const& fragment : mftItem->fragments)
		{
			if (fragment.fragment_count <= 0) continue;

			ifile.seekg(fragment.fragment_start_address);

			auto totalFragmentSize = _bootRecord.cluster_size * fragment.fragment_count;

			auto size = mftItem->item_size > totalFragmentSize ? totalFragmentSize : mftItem->item_size;

			char buffer[size];

			ifile.read(buffer, size);

			for (uint32_t i = 0; i < size; i++) result.push_back(buffer[i]);
		}
	}

	ifile.close();

	return result;
}

std::string Filesystem::check(std::vector<mft_item> item, std::vector<std::vector<mft_item>> items)
{
	auto logger = spdlog::get("all");
	uint64_t totalItemSize = 0;
	uint64_t totalFragmentSize = 0;

	logger->info("Checking mft item [uid: {} name: {}]", item[0].uid, item[0].item_name);

	// check size
	for (auto const& mftItem : item)
	{
		totalItemSize += mftItem.item_size;

		for (auto const& fragment : mftItem.fragments)
		{
			if (fragment.fragment_count <= 0) continue;

			totalFragmentSize += (fragment.fragment_count * CLUSTER_SIZE);
		}
	}

	if (totalFragmentSize < totalItemSize)
	{
		logger->error("Checking mft item [uid: {} name: {}] FAILED! Difference in size [total fragment size: {} total item size: {}]", item[0].uid, item[0].item_name, totalFragmentSize, totalItemSize);
		return "CHECK SIZE FAILED [uid: " + std::to_string(item[0].uid) + " name: " + item[0].item_name + "]\n";
	}

	// check parent directory
	for (auto const& directory : items)
	{
		if (!directory[0].isDirectory) continue;

		std::vector<uint32_t> content;

		std::ifstream ifile(_filename, std::ios::binary | std::ios::in);

		for (auto const& mftItem : directory)
		{
			auto totalSize = mftItem.item_size;

			content.clear();

			for (auto const& fragment : mftItem.fragments)
			{
				if (fragment.fragment_count > 0)
				{
					ifile.seekg(fragment.fragment_start_address);

					uint64_t totalFragmentSize = CLUSTER_SIZE * fragment.fragment_count;

					if (totalSize > totalFragmentSize) totalSize -= totalFragmentSize;

					uint64_t length = totalFragmentSize / sizeof(uint32_t);

					int32_t uids[length];

					ifile.read((char*)&uids, totalFragmentSize);

					for (auto const& uid : uids) if (uid > 0) content.push_back(uid);
				}
			}

		}

		ifile.close();

		for (auto const& uid : content) if (uid == item[0].uid) return std::string();
	}

	logger->error("Checking mft item [uid: {} name: {}] FAILED! Item is not in directory.", item[0].uid, item[0].item_name);
	return "CHECK PARENT DIRECTORY FAILED [uid: " + std::to_string(item[0].uid) + " name: " + item[0].item_name + "]\n";
}