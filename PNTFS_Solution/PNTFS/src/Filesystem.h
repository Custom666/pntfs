#ifndef Filesystem_H
#define Filesystem_H

#include <string>
#include <vector>
#include <cstdint>
#include <stdlib.h>
#include <map>

const int32_t MFT_FRAGMENTS_COUNT = 32;
const uint32_t CLUSTER_SIZE = 4096;

struct boot_record
{
	char signature[9];               //login autora FS
	char volume_descriptor[251];     //popis vygenerovan�ho FS
	int64_t disk_size;               //celkova velikost FS
	int32_t cluster_size;            //velikost clusteru
	int32_t cluster_count;           //pocet clusteru
	uint64_t mft_start_address;      //adresa pocatku mft
	uint64_t bitmap_start_address;   //adresa pocatku bitmapy
	uint64_t data_start_address;     //adresa pocatku datovych bloku
	uint32_t mft_max_fragment_count; //maximalni pocet fragmentu v jednom zaznamu v mft (pozor, ne souboru)
									 // stejne jako   MFT_FRAGMENTS_COUNT
	const std::string ToString() { return "disk size: " + std::to_string(disk_size) + "\ncluster size: " + std::to_string(cluster_size) + "\ncluster count: " + std::to_string(cluster_count); }
};

struct mft_fragment
{
	uint64_t fragment_start_address;     //start adresa
	uint32_t fragment_count;             //pocet clusteru ve fragmentu
};

struct mft_item
{
	uint32_t uid;                                       //UID polozky
	bool isDirectory;                                   //soubor, nebo adresar
	uint32_t item_order;                                //poradi v MFT pri vice souborech, jinak 1
	uint32_t item_order_total;                          //celkovy pocet polozek v MFT
	char item_name[12];                                 //8+3 + /0 C/C++ ukoncovaci string znak
	uint64_t item_size;                                 //velikost souboru v bytech
	struct mft_fragment fragments[MFT_FRAGMENTS_COUNT]; //fragmenty souboru

	std::string ToString() { return std::string(item_name) + " - " + std::to_string(uid) + " - " + std::to_string(item_size) + " - FRAGMENTY - CLUSTERY"; };
};

class Filesystem
{

public:

	/*
	** Create new pseudo filesystem with structure from given file.
	*/
	Filesystem(std::string filename);
	~Filesystem();

	/*
	** Create new structure from given size and write it into input file.
	*/
	bool Format(uint32_t size, std::string units);
	
	/*
	** Print all items in directory by given path.
	*/
	void ListDirectory(std::vector<std::string> path);
	
	/*
	** Change current directory by given path.
	*/
	void ChangeDirectory(std::vector<std::string> path);
	
	/*
	** Make new directory in given path.
	*/
	void MakeDirectory(std::vector<std::string> path);
	
	/*
	** Remove directory from given path.
	*/
	void RemoveDirectory(std::vector<std::string> path);
	
	/*
	** Return absolute path of current directory.
	*/
	std::string PrintWorkingDirectory();
	
	/*
	** Return each mft item and its fragments information by given path.
	*/
	std::string Info(std::vector<std::string> path);

	/*
	** Copy source file from hard disk into pseudo ntfs destination path.
	*/
	void InCopy(std::vector<std::string> source, std::vector<std::string> destination);
	
	/*
	** Copy pseudo ntfs file into destination path.
	*/
	void OutCopy(std::vector<std::string> source, std::vector<std::string> destination);
	
	/*
	** Remove file by given path.
	*/
	void RemoveFile(std::vector<std::string> path);
	
	/*
	** Print content of file by given path.
	*/
	std::string ConcatenateAndPrint(std::vector<std::string> path);
	
	/*
	** Copy file in pseudo ntfs from source path into destination path.
	*/
	void Copy(std::vector<std::string> source, std::vector<std::string> destination);
	
	/*
	** Move file in pseudo ntfs from source path into destination path.
	*/
	void Move(std::vector<std::string> source, std::vector<std::string> destination);

	/*
	** Check if pseudo ntfs is valid (check each mft item size and parentise).
	*/
	void Check();

	/*
	** Corrupt size of mft item by given path to simulate invalid state for check command.
	*/
	void CorruptSize(std::vector<std::string> path);

	/*
	** Return current directory as mft items.
	*/
	std::vector<mft_item*> GetCurrentDirectory();

private:
	static std::string _filename; // path of input file
	
	uint32_t _currentDirectory; // mft item uid of current directory

	boot_record _bootRecord; // pseudo ntfs boot record structure
	
	mft_item* _mftTable; // array of mft items
	uint64_t _mftLength; // length of mft table

	bool* _bitmap; // array of clusters state (use/unuse)
	uint64_t _bitmapLength; // length of bitmap

	// Return mft items of given path file/directory. If not exist return empty vector.
	std::vector<mft_item*> pathExist(std::vector<std::string> path);

	// Return mft items by given name relative to given directory. If not exist return empty vektor.
	std::vector<mft_item*> itemExist(std::string itemName, std::vector<mft_item*> relativeTo);
	
	// Genereate new mft item uid as (current maximum uid + 1)
	int32_t generateUID(); 

	// Return mft items by uid.
	std::vector<mft_item*> getMftItem(uint32_t uid);

	// Create new mft item
	void createMftItem(std::string name, bool isDirectory, std::vector<mft_item*> parentDirectory, char* data, uint64_t dataLength);
	
	// Remove mft item
	void removeMftItem(std::vector<mft_item*> itemToRemove, std::vector<mft_item*> parentDirectory);
	
	// Get directory content as vektor of mft items
	std::vector<std::vector<mft_item*>> getDirectoryContent(std::vector<mft_item*> directory);
	
	// Get file content as vektor of chars
	std::vector<char> getFileContent(std::vector<mft_item*> file);

	// Check given mft item size and parentise against all mft items. Designed for multi-threading.
	static std::string check(std::vector<mft_item> item, std::vector<std::vector<mft_item>> items);
};

#endif